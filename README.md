# Micro Tools

## Description

Wouldn't it be great to have a library of compiled executables that you can just drop into your project and use? That's what micro-tools is for. It's a collection of tools that can be used to make your life easier.

## Contributing

For people who want to make changes or contribute to this project, it's helpful to have some documentation on how to get started. It would be great to have a Contributing.md file that explains how to get started. And this could be a good place for a contributor to start.

Another way to contribute is to add a tool that you use. If you use a tool that you think would be useful to others, please add it to the list of tools below.

Finally you can contribute through [BMC](https://buymeacoffee.com/tragopoulos).

## License

Everything in this repository is released under the MIT license. See LICENSE for more information. Of course this doesn't apply to the Go Compiler and Standard Library which is licensed under a [BSD license](https://pkg.go.dev/std?tab=licenses).
